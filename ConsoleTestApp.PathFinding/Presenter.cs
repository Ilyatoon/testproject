using ConsoleTestApp.Core.Domain;
using ConsoleTestApp.RouteFinding.Abstract;
using ConsoleTestApp.RouteFinding.Service.Abstract;
using ConsoleTestApp.RouteFinding.ViewData;

namespace ConsoleTestApp.RouteFinding;
public class Presenter : IPresenter
{
    private readonly IView view;
    private readonly ISchemeService schemeService;
    private readonly IRouteFindingService routeFindingService;
    public Presenter(IView view, ISchemeService schemeService, IRouteFindingService routeFindingService) {
        this.view = view;
        this.schemeService = schemeService;
        this.routeFindingService = routeFindingService;
        GetAndShowSection();
        RequiredSectionIds();
    }
    protected Scheme Scheme { get; private set; }

    private void GetAndShowSection() {
        Scheme = schemeService.GetScheme();
        var sections = Scheme.Sections.Select(x => new SectionShortInfo(x.Id, x.Name)).ToArray();
        view.ShowSection(sections);
    }

    private void RequiredSectionIds() {
        var requiredString = view.RequiredSectionIds();
        var requiredsectionIds = new int[2];
        var errorText = "";
        var routeSectionIds = new List<int>();

        if (ParseAndCheckSectionIds(ref requiredsectionIds, ref errorText, requiredString)) {
            if (routeFindingService.FindRoute(Scheme, requiredsectionIds[0], requiredsectionIds[1], ref routeSectionIds)) {
               // view.ShowRoute(Scheme.Sections.Where(x => routeSectionIds.Contains(x.Id)).Select(x => new SectionShortInfo(x.Id, x.Name)).ToArray());
                view.ShowRoute(GetSectionShortInfos(routeSectionIds, Scheme.Sections));             
            }
            else {
                view.ShowError(errorText);
            }           
        }
        else {
            view.ShowError(errorText);
        }

        RequiredSectionIds();
    }

    private bool ParseAndCheckSectionIds(ref int[] requiredsectionIds, ref string errorText, string requiredString) {
        if (!TryParse(ref requiredsectionIds, ref errorText, requiredString)) {            
            return false;
        }

        if(!CheckExistenceSectionIds(requiredsectionIds, ref errorText)) {
            
            return false;
        }

        return true;
    }

    private bool TryParse(ref int[] requiredsectionIds, ref string errorText, string str) {
        if(string.IsNullOrEmpty(str)) {
            errorText = "Строка пуста";
            return false;
        }

        var stringArray = str.Split(' ');
        if(stringArray.Length < 2 ) {
            errorText = "Строка содержит менее 2 элементов";
            return false;
        }

        for(int i=0; i<2; i++) {
            if (!int.TryParse(stringArray[i], out requiredsectionIds[i])) {
                errorText = $"Невозможно преобразовать значение {stringArray[i]}";
                return false;
            }
        }

        return true;
    }

    private bool CheckExistenceSectionIds(int[] requiredsectionIds, ref string errorText) {
        if(requiredsectionIds.All(x => Scheme.Sections.Any(s => s.Id == x))) {
            return true;
        }
        errorText = "Как минимум один порядковый номер участка отсутствует на схеме";
        return false;
    }

    private SectionShortInfo[] GetSectionShortInfos(List<int> routeSectionIds, IList<Section> sections) {
        var resultList = new List<SectionShortInfo>(routeSectionIds.Count);
        foreach(var sectionId in routeSectionIds) {
            var section = sections.FirstOrDefault(x => x.Id == sectionId);
            resultList.Add(new SectionShortInfo(section.Id, section.Name));
        }

        return resultList.ToArray();
    }
}
