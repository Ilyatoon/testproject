namespace ConsoleTestApp.RouteFinding.ViewData;
public class SectionShortInfo
{
    public SectionShortInfo(int id, string name) {
        Id = id;
        Name = name;
    }

    public int Id { get; }
    public string Name { get; }
}
