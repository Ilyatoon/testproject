using ConsoleTestApp.RouteFinding.ViewData;

namespace ConsoleTestApp.RouteFinding.Abstract;
public interface IView
{
    void ShowSection(SectionShortInfo[] sections);
    void ShowRoute(SectionShortInfo[] sections);
    void ShowError(string error);
    string? RequiredSectionIds();
}
