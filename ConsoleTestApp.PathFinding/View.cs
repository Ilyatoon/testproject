using ConsoleTestApp.RouteFinding.Abstract;
using ConsoleTestApp.RouteFinding.ViewData;

namespace ConsoleTestApp.RouteFinding;
public class View : IView
{
    public View() { }

    public string? RequiredSectionIds() {
        Console.WriteLine("Введите номер начального и конечного участка пути(через пробел)");
        return Console.ReadLine();
    }

    public void ShowRoute(SectionShortInfo[] sections) {
        foreach(var section in sections.Reverse()) {
            Console.WriteLine($"{section.Id}({section.Name}) -> ");
        }
        Console.WriteLine();
    }

    public void ShowError(string error) {
        Console.WriteLine($"Не удалось выполнить поиск пути по причине: {error}");
    }

    public void ShowSection(SectionShortInfo[] sections) {
        Console.WriteLine("Id   Имя");
        foreach(var section in sections) {
            Console.WriteLine($"{section.Id}   {section.Name}");
        }
    }
}
