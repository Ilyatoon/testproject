namespace ConsoleTestApp.RouteFinding.Service.Data;
public class RouteToPointInfo
{
    public RouteToPointInfo(int prevPointId, int prevSectionId, double distance) {
        PrevPointId = prevPointId;
        Distance = distance;
        PrevSectionId = prevSectionId;
    }

    public int PrevPointId { get; set; }
    public int PrevSectionId { get; set; }
    public double Distance { get; set; }
}
