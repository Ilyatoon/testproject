using ConsoleTestApp.Core.Domain;

namespace ConsoleTestApp.RouteFinding.Service.Abstract;
public interface ISchemeService
{
    public Scheme GetScheme();
}
