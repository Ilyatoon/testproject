using ConsoleTestApp.Core.Domain;

namespace ConsoleTestApp.RouteFinding.Service.Abstract;
public interface IRouteFindingService
{
    public bool FindRoute(Scheme scheme, int startSectionId, int finishSectionId, ref List<int> routeSectionIds);
}
