using ConsoleTestApp.Core.Domain;
using ConsoleTestApp.RouteFinding.Service.Abstract;
using ConsoleTestApp.RouteFinding.Service.Data;

namespace ConsoleTestApp.RouteFinding.Service;
public class RouteFindingService : IRouteFindingService
{
    public bool FindRoute(Scheme scheme, int startSectionId, int finishSectionId, ref List<int> routeSectionIds) {
        var startPoints = scheme.Sections[startSectionId].Points;
        var finishPoints = scheme.Sections[finishSectionId].Points;

        var pointRouteDictionary = new Dictionary<int, RouteToPointInfo>();

        pointRouteDictionary.Add(startPoints[0].Id, new RouteToPointInfo(startPoints[0].Id, startSectionId, 0));
        pointRouteDictionary.Add(startPoints[1].Id, new RouteToPointInfo(startPoints[1].Id, startSectionId, 0));

        foreach (var startPoint in startPoints) {
            SetRouteDictionary(pointRouteDictionary, startPoint);
        }

        var nearestFinishPointId = 0;
        if (GetNearestFinishPoint(pointRouteDictionary, finishPoints, ref nearestFinishPointId)) {
            routeSectionIds = GetRouteSectionIds(nearestFinishPointId, startSectionId, pointRouteDictionary);
            return true;
        }

        return false;
    }

    private List<int> GetRouteSectionIds(int nearestFinishPointId, int startSectionId, Dictionary<int, RouteToPointInfo> pointRouteDictionary) {
        var sectionIdsRoute = new List<int>();
        var pointId = nearestFinishPointId;
        while (pointRouteDictionary[pointId].PrevSectionId != startSectionId) {
            sectionIdsRoute.Add(pointRouteDictionary[pointId].PrevSectionId);
            pointId = pointRouteDictionary[pointId].PrevPointId;
        }
        return sectionIdsRoute;
    }

    private bool GetNearestFinishPoint(Dictionary<int, RouteToPointInfo> pointRouteDictionary, Point[] finishPoints, ref int nearestFinishPointId) {
        var routeToFinishPointsInfo = pointRouteDictionary.Where(x => finishPoints.Any(y => y.Id == x.Key));

        if(!routeToFinishPointsInfo.Any()) {
            return false;
        }

        var shortestDistance = routeToFinishPointsInfo.Min(x => x.Value.Distance);
        var shortestFinishPointRoute = routeToFinishPointsInfo.First(r => r.Value.Distance == shortestDistance);
        nearestFinishPointId = shortestFinishPointRoute.Key;
        return true;
    }

    private void SetRouteDictionary(Dictionary<int, RouteToPointInfo> pointRouteDictionary, Point startPoint) {
        var startPointRouteInfo = pointRouteDictionary[startPoint.Id];
        foreach (var section in startPoint.SectionOwners) {
            var secondPoint = section.Points.FirstOrDefault(x => x.Id != startPoint.Id);

            if (pointRouteDictionary.ContainsKey(secondPoint.Id)) {
                if (pointRouteDictionary[secondPoint.Id].Distance > pointRouteDictionary[startPoint.Id].Distance + section.Length) {
                    pointRouteDictionary[secondPoint.Id].Distance = pointRouteDictionary[startPoint.Id].Distance + section.Length;
                    pointRouteDictionary[secondPoint.Id].PrevSectionId = section.Id;
                    pointRouteDictionary[secondPoint.Id].PrevPointId = startPoint.Id;
                    SetRouteDictionary(pointRouteDictionary, secondPoint);
                }
            }
            else {
                pointRouteDictionary.Add(secondPoint.Id, new RouteToPointInfo(startPoint.Id, section.Id, pointRouteDictionary[startPoint.Id].Distance + section.Length));
                SetRouteDictionary(pointRouteDictionary, secondPoint);
            }
        }
    }
}
