using ConsoleTestApp.Core.Domain;
using ConsoleTestApp.Core.Service;
using ConsoleTestApp.RouteFinding.Service.Abstract;

namespace ConsoleTestApp.RouteFinding.Service;
public class SchemeService: ISchemeService
{
    private readonly ISchemeCreator schemeCreator;
    public SchemeService(ISchemeCreator schemCreator) {
        schemeCreator = schemCreator;
    }
    public Scheme GetScheme() {
        return schemeCreator.GetScheme();
    }
}
