using ConsoleTestApp.Core.Domain;
using ConsoleTestApp.ParkFilling.Service;

namespace ConsoleTestApp.ParkFillingTest;
public class FillingServiceTest
{
    [Test]
    public void GetFillingPoints_Add6Point_3Returned() {
        var service = new FillingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 0, 2), new Point(2, 0, 3), new Point(3, 0, 4), new Point(4, 0, 7), new Point(4, 1, 5) };

        var res = service.GetFillingPoints(points);
        Assert.That(res.Count, Is.EqualTo(3));
    }

    [Test]
    public void GetFillingPoints_Add2Point_2Returned() {
        var service = new FillingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 0, 2)};

        var res = service.GetFillingPoints(points);
        Assert.That(res.Count, Is.EqualTo(2));
    }
    [Test]
    public void GetFillingPoints_Ad5PointOnOneLine_2Returned() {
        var service = new FillingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 0, 2), new Point(2, 0, 3), new Point(3, 0, 4), new Point(4, 0, 7) };

        var res = service.GetFillingPoints(points);
        Assert.That(res.Count, Is.EqualTo(2));
    }
    [Test]
    public void GetFillingPoints_Ad6PointSqueareForm_4Returned() {
        var service = new FillingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 0, 4), new Point(2, 4, 0), new Point(3, 4, 4), new Point(4, 2, 2), new Point(4, 3, 3) };

        var res = service.GetFillingPoints(points);
        Assert.That(res.Count, Is.EqualTo(4));
    }
}

