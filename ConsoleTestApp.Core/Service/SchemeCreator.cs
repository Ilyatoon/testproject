using ConsoleTestApp.Core.Domain;


namespace ConsoleTestApp.Core.Service;
public class SchemeCreator : ISchemeCreator
{
    public Scheme GetScheme() {
        var points = GetPoints();
        var sections = GetSections(points);
        var paths = GetPaths(sections);
        var parks = GetParks(paths);

        return new Scheme(0, "Схема 1", sections, paths, parks);
    }


    private Park[] GetParks(Domain.Path[] paths) {
        return new Park[] {
            new Park(0, "Парк 1", new Domain.Path[]{paths[0], paths[1]}),
            new Park(1, "Парк 2", new Domain.Path[]{paths[2], paths[3]}),
            new Park(2, "Парк 3", new Domain.Path[]{paths[5], paths[6],paths[7]})
        };
    }

    private Domain.Path[] GetPaths(Section[] sections) {
        return new Domain.Path[]{
            new Domain.Path(0, "Путь А", new Section[]{sections[19], sections[24]}),
            new Domain.Path(1, "Путь Б", new Section[]{sections[16], sections[23],sections[28]}),
            new Domain.Path(2, "Путь В", new Section[]{sections[9], sections[34]}),
            new Domain.Path(3, "Путь Г", new Section[]{sections[22]}),
            new Domain.Path(4, "Путь Д", new Section[]{sections[3], sections[8], sections[14], sections[21]}),
            new Domain.Path(5, "Путь Е", new Section[]{sections[7], sections[12], sections[17]}),
            new Domain.Path(6, "Путь Ё", new Section[]{sections[27], sections[30], sections[33]}),
            new Domain.Path(7, "Путь Ж", new Section[]{sections[26], sections[29], sections[32]}),
        };
    }
    private Section[] GetSections(Point[] points) {
        return new Section[] {
            new Section(0,points[0],points[2]),
            new Section(1,points[1],points[3]),
            new Section(2,points[2],points[4]),
            new Section(3,points[2],points[5]),
            new Section(4,points[3],points[5]),
            new Section(5,points[3],points[7]),
            new Section(6,points[3],points[6]),
            new Section(7,points[4],points[8]),
            new Section(8,points[5],points[10]),
            new Section(9,points[6],points[34]),
            new Section(10,points[7],points[11]),
            new Section(11,points[8],points[9]),
            new Section(12,points[8],points[13]),
            new Section(13,points[9],points[22]),
            new Section(14,points[10],points[15]),
            new Section(15,points[34],points[16]),
            new Section(16,points[12],points[17]),
            new Section(17,points[13],points[18]),
            new Section(18,points[13],points[15]),
            new Section(19,points[14],points[19]),
            new Section(20,points[15],points[20]),
            new Section(21,points[15],points[23]),
            new Section(22,points[16],points[21]),
            new Section(23,points[17],points[24]),
            new Section(24,points[19],points[25]),
            new Section(25,points[20],points[22]),
            new Section(26,points[20],points[27]),
            new Section(27,points[22],points[28]),
            new Section(28,points[24],points[26]),
            new Section(29,points[27],points[29]),
            new Section(30,points[28],points[31]),
            new Section(31,points[29],points[33]),
            new Section(32,points[29],points[30]),
            new Section(33,points[31],points[32]),
            new Section(34,points[34],points[11]),
            new Section(35,points[17],points[19]),
            new Section(36,points[34],points[12])
        };
    }
    private Point[] GetPoints() {
        return new Point[] {
            new Point(0,1,5),
            new Point(1,1,6),
            new Point(2,3,5),
            new Point(3,3,6),
            new Point(4,4,3),
            new Point(5,4.5,5),
            new Point(6,4.5,8),
            new Point(7,6,6),
            new Point(8,6.5,3),
            new Point(9,7,2),
            new Point(10,7,5),
            new Point(11,7,6),
            new Point(12,7,11),
            new Point(13,8,3),
            new Point(14,8,12),
            new Point(15,9,5),
            new Point(16,9,8),
            new Point(17,9,11),
            new Point(18,9.5,3),
            new Point(19,9.5,12),
            new Point(20,10.5,3),
            new Point(21,10.5,8),
            new Point(22,11.5,2),
            new Point(23,11.5,5),
            new Point(24,11.5,11),
            new Point(25,12,12),
            new Point(26,12.5,8),
            new Point(27,13.5,3),
            new Point(28,14,2),
            new Point(29,14,5),
            new Point(30,14.5,5.5),
            new Point(31,15,1),
            new Point(32,16,2),
            new Point(33,16,5),
            new Point(34,6.5,8)
        };
    }
}
