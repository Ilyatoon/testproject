using ConsoleTestApp.Core.Domain;

namespace ConsoleTestApp.Core.Service;
public interface ISchemeCreator
{
    public Scheme GetScheme();
}
