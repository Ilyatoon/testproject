namespace ConsoleTestApp.Core.Extension;
public static class IListExstension
{
    public static void Swap<T>(this IList<T> list, int indexA, int indexB) {
        (list[indexB], list[indexA]) = (list[indexA], list[indexB]);
    }
}
