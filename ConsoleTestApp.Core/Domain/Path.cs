namespace ConsoleTestApp.Core.Domain;
public class Path
{
    public Path(int id, string name, Section[] sections) {
        Id = id;
        Name = name;
        Sections = new List<Section>(sections);
    }

    public int Id { get; }
    public string Name { get; }
    public IEnumerable<Section> Sections { get; set; }
}
