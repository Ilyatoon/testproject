namespace ConsoleTestApp.Core.Domain;
public class Scheme
{
    public Scheme(int id, string name, IList<Section> sections, IList<Path> paths, IList<Park> parks) {
        Id = id;
        Name = name;
        Sections = sections;
        Paths = paths;
        Parks = parks;
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public IList<Section> Sections { get; set; }
    public IList<Path> Paths { get; set; }
    public IList<Park> Parks { get; set; }
}
