namespace ConsoleTestApp.Core.Domain;
public class Point
{
    public Point(int id, double x, double y) {
        Id = id;
        X = x;
        Y = y;
        SectionOwners = new List<Section>();
    }

    public int Id { get; }
    public double X { get; }
    public double Y { get; }
    public IList<Section> SectionOwners { get; set; }

}

