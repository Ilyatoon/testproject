namespace ConsoleTestApp.Core.Domain;
public class Section
{
    public Section(int id, Point firstPoint, Point secondPoint) {
        Id = id;
        Name = GetName(firstPoint.Id, secondPoint.Id);
        Points = new Point[2] {firstPoint, secondPoint};
        Length = GetLength();

        firstPoint.SectionOwners.Add(this);
        secondPoint.SectionOwners.Add(this);
    }
    public int Id { get; }
    public string Name { get; }
    public Point[] Points { get; } 
    public double Length { get; }
    private string GetName(int firstPointId, int secondPointId) {
        return $"{firstPointId}_{secondPointId}";
    }
    private double GetLength() {
        return Math.Round(Math.Sqrt(Math.Pow(Points[0].X- Points[1].X,2)+ Math.Pow(Points[0].Y - Points[1].Y, 2)),2);
    }
}
