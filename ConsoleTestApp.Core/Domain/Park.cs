namespace ConsoleTestApp.Core.Domain;
public  class Park
{
    public Park(int id, string name, Path[] paths) {
        Id = id;
        Name = name;
        Paths = new List<Path>(paths);
    }

    public int Id { get; }
    public string Name { get; }
    public IEnumerable<Path> Paths { get; set; }

    public IList<Point> GetPoints() {
        return Paths.SelectMany(x => x.Sections).SelectMany(x => x.Points).Distinct().ToList();
    }
}
