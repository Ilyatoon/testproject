using ConsoleTestApp.Core.Domain;
using ConsoleTestApp.RouteFinding.Service;


namespace ConsoleTestApp.RouteFindingTest;
public class RouteFindingServiceTest
{
    [Test]
    public void FindRoute_AddUnconnectedSections_FalseReturned() {
        var service = new RouteFindingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 0, 2), new Point(2, 2, 3), new Point(3,2, 4), new Point(4, 2, 7), };
        var sections = new Section[] { new Section(0, points[0], points[1]), new Section(1, points[2], points[3]), new Section(2, points[3], points[4]) };
        var schema = new Scheme(0, "", sections, null, null);
        var result = new List<int>();

        var res = service.FindRoute(schema, 2, 0, ref result);
        Assert.That(res, Is.EqualTo(false));
    }

    [Test]
    public void FindRoute_AddConnectedSections_TrueReturned() {
        var service = new RouteFindingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 0, 2), new Point(2, 2, 3), new Point(3, 2, 4),};
        var sections = new Section[] { new Section(0, points[0], points[1]), new Section(1, points[1], points[2]), new Section(2, points[2], points[3]) };
        var schema = new Scheme(0, "", sections, null, null);
        var result = new List<int>();

        var res = service.FindRoute(schema, 2, 0, ref result);
        Assert.That(res, Is.EqualTo(true));
    }
    [Test]
    public void FindRoute_AddTwoWaysClose_0SectionsReturned() {
        var service = new RouteFindingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 4, 10), new Point(2, 5, 0), new Point(3, 2, 0.5), new Point(4, 4, 0.25), };
        var sections = new Section[] { new Section(0, points[0], points[1]), new Section(1, points[1], points[2]), new Section(2, points[0], points[3]), new Section(3, points[3], points[4]), new Section(4, points[4], points[2]),};
        var schema = new Scheme(0, "", sections, null, null);
        var result = new List<int>();

        var res = service.FindRoute(schema, 0, 2, ref result);
        Assert.That(result.Count, Is.EqualTo(0));
    }
    [Test]
    public void FindRoute_AddTwoWays_3SectionsReturned() {
        var service = new RouteFindingService();
        var points = new Point[] { new Point(0, 0, 0), new Point(1, 4, 100), new Point(2, 5, 0), new Point(3, 2, 0.5), new Point(4, 4, 0.25), new Point(5, 5, 6) };
        var sections = new Section[] { new Section(0, points[0], points[1]), new Section(1, points[1], points[5]), new Section(2, points[0], points[3]), new Section(3, points[3], points[4]), new Section(4, points[4], points[2]), new Section(5, points[5], points[2]) };
        var schema = new Scheme(0, "", sections, null, null);
        var result = new List<int>();

        var res = service.FindRoute(schema, 0, 5, ref result);
        Assert.That(result.Count, Is.EqualTo(3));
    }
}
