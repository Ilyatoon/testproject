using ConsoleTestApp.Core.Service;
using ConsoleTestApp.ParkFilling;
using ConsoleTestApp.ParkFilling.Abstract;
using ConsoleTestApp.ParkFilling.Service;
using ConsoleTestApp.ParkFilling.Service.Abstract;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


var host = Host.CreateDefaultBuilder().ConfigureServices(services => {
    services.AddSingleton<IPresenter, Presenter>();
    services.AddScoped<IView, View>();
    services.AddScoped<ISchemeCreator, SchemeCreator>();
    services.AddScoped<ISchemeService, SchemeService>();
    services.AddScoped<IFillingService, FillingService>();
})
.Build();

var presenter = host.Services.GetRequiredService<IPresenter>();