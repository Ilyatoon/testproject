using ConsoleTestApp.ParkFilling.ViewPata;

namespace ConsoleTestApp.ParkFilling.Abstract;
public interface IView
{
    public void ShowParksFillInfo(IEnumerable<ParkFillInfo> parkFillInfoArray);
}
