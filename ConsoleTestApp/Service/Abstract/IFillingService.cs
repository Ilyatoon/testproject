using ConsoleTestApp.Core.Domain;

namespace ConsoleTestApp.ParkFilling.Service.Abstract;
public interface IFillingService
{
    public IList<Point> GetFillingPoints(IList<Point> points);
}
