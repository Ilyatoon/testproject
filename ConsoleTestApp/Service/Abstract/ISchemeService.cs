using ConsoleTestApp.Core.Domain;

namespace ConsoleTestApp.ParkFilling.Service.Abstract;
public interface ISchemeService
{
    public Scheme GetScheme();
}
