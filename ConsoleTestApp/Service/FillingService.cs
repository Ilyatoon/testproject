using ConsoleTestApp.Core.Domain;
using ConsoleTestApp.Core.Extension;
using ConsoleTestApp.ParkFilling.Service.Abstract;

namespace ConsoleTestApp.ParkFilling.Service;
public class FillingService : IFillingService
{

    public IList<Point> GetFillingPoints(IList<Point> points) {

        var startPoint = GetStartPoint(points);
        SortPoints(points, startPoint);

        return FilterFillingPoints(points);
    }

    private IList<Point> FilterFillingPoints(IList<Point> points) {
        var fillingPoints = new List<Point>() { points[0], points[1] };
        var lastIndex = fillingPoints.Count - 1;
        for (int i = 2; i < points.Count; i++) {
            while (lastIndex > 0 && GetTurningRatio(fillingPoints[lastIndex - 1], fillingPoints[lastIndex], points[i]) >= 0) {
                fillingPoints.Remove(fillingPoints[lastIndex]);
                lastIndex--;
            }
            fillingPoints.Add(points[i]);
            lastIndex++;
        }
        return fillingPoints;
    }

    private Point GetStartPoint(IEnumerable<Point> points) {
        var minXValue = points.Min(x => x.X);
        var minXPoints = points.Where(x => x.X == minXValue);

        if (minXPoints.Count() == 1) {
            return minXPoints.ElementAt(0);
        }

        var minYValue = minXPoints.Min(x => x.Y);
        return minXPoints.FirstOrDefault(x => x.Y == minYValue);
    }

    private void SortPoints(IList<Point> points, Point startPoint) {
        points.Swap(0, points.IndexOf(startPoint));
        var pointCount = points.Count();

        for (int i = 1; i < pointCount; i++) {
            var j = i;
            while ((j > 0) && GetTurningRatio(startPoint, points[j], points[j - 1]) < 0) {
                (points[j], points[j - 1]) = (points[j - 1], points[j]);
                j--;
            }
        }

    }
    private double GetTurningRatio(Point startPoint, Point secondPoint, Point thirdPoint) {
        return (thirdPoint.X - startPoint.X) * (secondPoint.Y - startPoint.Y) - (thirdPoint.Y - startPoint.Y) * (secondPoint.X - startPoint.X);
    }
}
