using ConsoleTestApp.ParkFilling.Abstract;
using ConsoleTestApp.ParkFilling.ViewPata;

namespace ConsoleTestApp.ParkFilling;
public class View : IView
{
    public void ShowParksFillInfo(IEnumerable<ParkFillInfo> parkFillInfoArray) {
        Console.WriteLine("Список доступных парков:");
        foreach(var parkInfo in parkFillInfoArray) {
            Console.WriteLine($"{parkInfo.ParkName} - {string.Join(",", parkInfo.FillPoint)}");
        }
        Console.ReadKey();
    }
}
