namespace ConsoleTestApp.ParkFilling.ViewPata;
public class ParkFillInfo
{
    public ParkFillInfo(string parkName, int[] fillPoint) {
        ParkName = parkName;
        FillPoint = fillPoint;
    }

    public string ParkName { get; }

    public int[] FillPoint { get; }
}
