using ConsoleTestApp.Core.Domain;
using ConsoleTestApp.ParkFilling.Abstract;
using ConsoleTestApp.ParkFilling.Service.Abstract;
using ConsoleTestApp.ParkFilling.ViewPata;

namespace ConsoleTestApp.ParkFilling;
public class Presenter : IPresenter
{
    private readonly IView view;
    private readonly ISchemeService schemeService;
    private readonly IFillingService fillingService;
    public Presenter(IView view, ISchemeService schemeService, IFillingService fillingService) {
        this.view = view;
        this.schemeService = schemeService;
        this.fillingService = fillingService;
        Scheme = schemeService.GetScheme();
        FillAndShowParks();
    }
    protected Scheme Scheme { get; private set; }
    private void FillAndShowParks() {
        var parks = Scheme.Parks;
        var parkFillInfoList = new List<ParkFillInfo>();
        foreach(var park in parks ) {
            var points = park.GetPoints();
            var pointIds = fillingService.GetFillingPoints(points).Select(x => x.Id).ToArray();
            parkFillInfoList.Add(new ParkFillInfo(park.Name, pointIds));
        }

        view.ShowParksFillInfo(parkFillInfoList);
    }
}
